<?PHP
require_once 'db.php';

echo '<p>Adressen</p>';

echo '<p>Hier is een lijst van toegevoegde adressen:</p>';

$query = $db->prepare('SELECT * FROM address');
$query->execute();
foreach ($query as $address){
    echo '<p>'.$address['street'].' '.$address['number'].' '.$address['zip'].' '.$address['city'].'</p>';
}