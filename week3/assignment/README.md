Assignment 3: Input validation, SQL-injection and XSS

This assignment is divided in 3 parts. The first part covers basis input validation, the second part is SQL-injection and the final part features XSS (Cross-site scripting).

On the command line, go into your `ddwt2017` directory, i.e. your local copy of
the repository that you created for the first assignment. Update it using the following
command.

    git pull

This adds a directory `week3/assignment/`, which contains the starter
code for this assignment.

You will now have a assignment directory consisting of one PHP script per part, a database-connection script and a SQL dump.

Enter your database credentials in db.php and import the database.sql file into the database. You are now ready to start.

Part 1: Input validation (part_1.php)
----------------------------
This assignment features a basic application containing the ability to enter addresses into a database. However, there is a lack of input validation.

Task is to fix the following two issues:

* you are currently able to enter letters as a street number, make sure you are only able to insert numeric values.
* you are currently able to enter a diverse format for the ZIP code (it should only accept a ZIP in the format: '1234 AB')

Display an appropriate message when incorrect values have been entered. Commit your changes to your repository.

Part 2: SQL injection (part_2.php)
----------------------------
The same application in a different format. We don't focus on input validation here, but our application does contain the ability to perform SQL-injections! We don't want that.

As you may have already seen, the database also contains the "users" table. Using only part_2.php (the form to insert new addresses), drop the users table through SQL-injection.

Open part_2.php in your browser and input values in the form causing the users table to be dropped.

Question 1: Describe what you did to drop the users table and write it down below:
------

Then fix the ability to perform SQL-injections by making changes to part_2.php and commit them to your repository. 

Part 3: XSS (Cross-site scripting) (part_3.php)
----------------------------
In part_3.php, all addresses from the database are being displayed. However, using the form of part_1.php, we are able to perform cross-site scripting within the application.

Question 2: Describe what you can input as an address to perform XSS and write it down below:
------

Then fix the ability to perform SQL-injections by making changes to part_3.php and commit them to your repository. 


Make sure to:
* Answer the two questions above by typing your answer in this README.md file
* Make sure to commit all your changes to README.md, part_1.php, part_2.php and part_3.php back to your own repository.
* Submit your repository URL through Nestor 

Grading
-------

The deadline is *Sunday, December 10, 23:59*.

Your work will be graded based on the following criteria:

* answered the questions listed above
* part_1.php contains input validation on the street number and ZIP
* part_2.php contains measures against SQL injection
* part_3.php will no longer have the ability to display XSS-code
* satisfies the functional requirements specified here
* code is legible (e.g. consistent indentation, meaningful variable names)

*Happy hacking!*
