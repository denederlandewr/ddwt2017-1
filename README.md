ddwt2017
========

Course repository for [Database-driven Webtechnology 2017/2018](https://www.rug.nl/ocasys/rug/vak/show?code=LIX021B05).

Lecture Slides
--------------

* [Week 1: Basics](https://ddwt.mark.dog/static/app/week-1.pdf)

Other materials can be found on https://ddwt.mark.dog
