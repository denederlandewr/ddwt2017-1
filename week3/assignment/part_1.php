<?PHP
require_once 'db.php';

echo '<p>Adres toevoegen</p>';

if (isset($_POST['street']) && isset($_POST['number']) && isset($_POST['zip']) && isset($_POST['city'])){
    $query = $db->prepare('INSERT INTO address (street, number, zip, city) VALUES (?, ?, ?, ?)');
    $query->execute(array($_POST['street'], $_POST['number'], $_POST['zip'], $_POST['city']));
    echo 'Adres toegevoegd';
}
echo '<form method="post" action="">';
echo '<input type="text" name="street" placeholder="Straatnaam" size="100" /><br>';
echo '<input type="text" name="number" placeholder="Nummer" size="100" /><br>';
echo '<input type="text" name="zip" placeholder="Postcode" size="100" /><br>';
echo '<input type="text" name="city" placeholder="Stad" size="100" /><br>';
echo '<input type="submit" value="Verzenden" />';
echo '</form>';
